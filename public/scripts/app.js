/*
 * The MIT License
 *
 * Copyright (c) 2014, Sebastian Sdorra
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

'use strict';

angular.module('myApp', [
    'adf', 'ngRoute', 'adf.structures.base'
  ])
  .config(function($routeProvider){
    // $routeProvider
    //   .when('/boards', {
    //     templateUrl: 'partials/default.html'
    //   })
    //   .when('/boards/:id', {
    //     controller: 'dashboardCtrl',
    //     controllerAs: 'dashboard',
    //     templateUrl: 'partials/dashboard.html',
    //     resolve: {
    //       data: function($route, storeService){
    //         return storeService.get($route.current.params.id);
    //       }
    //     }
    //   })
    //   .otherwise({
    //     redirectTo: '/boards'
    //   });
  })
  .service('priceService', function($http, $q){
    return {
      search: function( searchString ){
        var deferred = $q.defer();
        $http.get('/price/search/' + searchString)
          .success(function(data){
            deferred.resolve(data);
          })
          .error(function(){
            deferred.reject();
          });
        return deferred.promise;
      },
      getBit: function(){
        var deferred = $q.defer();
        $http.get('/price/bit')
          .success(function(data){
            deferred.resolve(data);
          })
          .error(function(){
            deferred.reject();
          });
        return deferred.promise;
      },
      setBit: function( data){
        var deferred = $q.defer();
        $http.post('/price/bit', data)
          .success(function(data){
            deferred.resolve();
          })
          .error(function(){
            deferred.reject();
          });
        return deferred.promise;
      },
      delete: function( data){
        var deferred = $q.defer();
        $http.delete('/price/bit/'+data.qurl)
          .success(function(data){
            deferred.resolve(data);
          })
          .error(function(err){
            deferred.reject(err);
          });
        return deferred.promise;
      },
      setBits: function ( data) {
        var deferred = $q.defer();
        $http.post('/price/allBit', data)
          .success(function(data){
            deferred.resolve();
          })
          .error(function(){
            deferred.reject();
          });
        return deferred.promise;
      }
    };
  })
  .controller('priceController', function($location, $rootScope, $scope, $routeParams, priceService){

    $scope.bitItems = [ {qurl:'test.com',basePrice:'10',bitPrice:'11'}]
    $scope.searchString = ''
    $scope.priceItems = []
    $scope.alerts = []

    priceService.getBit().then(function (data) {
      console.log(data)
      $scope.bitItems = data
    }, function (err) {
      console.log(err)
    })

    $scope.search = function ( ) {
      console.log('search'+$scope.searchString)
      if ($scope.searchString != '') {
              priceService.search($scope.searchString).then ( function (data) {
                console.log(data)
                $scope.priceItems = data
                var alert = { type: 'success', msg: "search success" }
                $scope.alerts=[alert];
              },function (err) {
        var alert = { type: 'danger', msg: "search failed" }
        $scope.alerts=[alert];
      })
      };
    }

    $scope.update = function ( bit ) {
      console.log('update')
      priceService.setBit(bit).then( function (data) {
        var alert = { type: 'success', msg: "update success" }
        $scope.alerts=[alert];
      },function (err) {
        var alert = { type: 'danger', msg: "update failed" }
        $scope.alerts=[alert];
      })
    }

    $scope.add = function (bit ) {
      console.log('add')
      priceService.setBit(bit).then( function (data) {
        var alert = { type: 'success', msg: "add success" }
        $scope.alerts=[alert];
        $scope.bitItems.push(bit)
      },function (err) {
        var alert = { type: 'danger', msg: "add failed" }
        $scope.alerts=[alert];
      })
    }

    $scope.remove = function (bit) {
      console.log('remove')
      priceService.delete(bit).then ( function (data) {
        var index = $scope.bitItems.indexOf(bit);
        if (index > -1) {
          $scope.bitItems.splice(index, 1);
        }
        var alert = { type: 'success', msg: "remove success" }
        $scope.alerts=[alert];

      },function (err) {
        var alert = { type: 'danger', msg: "remove failed" }
        $scope.alerts=[alert];
      })
    }

    $scope.refresh = function () {
      console.log('refresh')
      priceService.getBit().then(function (data) {
        $scope.bitItems = data
        var alert = { type: 'success', msg: "refresh success" }
        $scope.alerts=[alert];
      },function (err) {
        var alert = { type: 'danger', msg: "refresh  failed" }
        $scope.alerts=[alert];
      })
    }

    $scope.save = function () {
      console.log('save')
      priceService.setBits($scope.bitItems).then (function (data) {
        var alert = { type: 'success', msg: "save success" }
        $scope.alerts=[alert];
      },function (err) {
        var alert = { type: 'danger', msg: "save failed" }
        $scope.alerts=[alert];
      })
    }
    $scope.closeAlert = function(index) {
      $scope.alerts.splice(index, 1);
    };

  });
