var Redis = require("ioredis")

var redis = new Redis(6379,'f40dff6071594d6d.m.cnsha.kvstore.aliyuncs.com',{password:'f40dff6071594d6d:ldqRDS11'});

module.exports = {
    getBit: function (req, res, next) {
    	redis.keys('*:bit').then(function(domains) {
            var priceDomains = domains.map(function (e, i, arr) {
                return e.substring(0,e.length-4) + ':price'
            })
            var bitcountDomains = domains.map(function (e, i, arr) {
                return e.substring(0,e.length-4) + ':bitcount'
            })
            var countDomains = domains.map(function (e, i, arr) {
                return e.substring(0,e.length-4) + ':count'
            })
            console.log(countDomains)

            redis.multi().mget(domains).mget(priceDomains).mget(countDomains).mget(bitcountDomains).exec(function(err,prices) {
                console.log(err,prices)
                var result = domains.map(function (e, i, arr) {
                    var item = {}
                    item.qurl = e.substring(0,e.length-4)
                    item.bitPrice = prices[0][1][i]
                    item.basePrice = prices[1][1][i]
                    item.count = prices[2][1][i]
                    item.bitcount = prices[3][1][i]
                    console.log(item)
                    return item
                })
                res.json(result)
            })
        })
    },
    setBit: function (req, res, next) {
        var bit = req.body
        redis.set(bit.qurl+':bit',bit.bitPrice).then(function(data){
        	res.status(200).end()
        }, function(err){
        	res.status(403).end()
        })
    },

    deleteBit: function (req, res, next) {
        var qurl = req.params.qurl
        redis.del(qurl+':bit').then(function(data){
        	console.log(qurl+':bit',data)
        	res.status(200).end()
        }, function(err){
        	res.status(404).end()
        })
    },

    searchPrice: function (req, res, next) {
    	var search = req.params.searchString

    	redis.keys('*'+search+'*:price').then(function(domains) {
            var countDomains = domains.map(function (e, i, arr) {
                return e.substring(0,e.length-6) + ':count'
            })
    		redis.multi().mget(domains).mget(countDomains).exec(function(err,prices) {
    			console.log(err,prices)
    			var result = domains.map(function (e, i, arr) {
    				var item = {}
    				item.qurl = e.substring(0,e.length-6)
    				item.bitPrice = prices[0][1][i]
    				item.basePrice = prices[0][1][i]
                    item.count = prices[1][1][i]
    				return item
    			})

    			res.json(result)
    		})
    	})
        
    },

    setBits: function (req, res, next) {
        res.status(500).end()
    },

};