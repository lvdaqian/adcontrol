var UsersController = require('./usersController');
var priceController = require('./priceController');

module.exports = {
  users: UsersController,
  price: priceController
};