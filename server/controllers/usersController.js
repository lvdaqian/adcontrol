var encryption = require('../utilities/cripto')

module.exports = {
    getLogin: function (req, res, next) {
        if (req.user) {
            res.redirect('/');
        }
        else {
            res.render('users/login');
        }
    }
};