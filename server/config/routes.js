var auth = require('./auth'),
    controllers = require('../controllers');

module.exports = function(app) {
    // app.get('/register', controllers.users.getRegister);
    // app.post('/register', controllers.users.createUser);

    app.post('/login', auth.login);
    app.get('/logout', auth.logout);
    app.get('/login', controllers.users.getLogin);

    app.get('/', function (req, res) {
        res.render('index', {currentUser: req.user});
    });

    app.get('/dashboard', function (req, res) {
        if (req.isAuthenticated()) {
            res.render('dashboard', {currentUser: req.user})
        }
        else {
            res.redirect('/login')
        }
    })
    app.get('/price/bit', controllers.price.getBit)
    app.post('/price/bit', controllers.price.setBit)
    app.delete('/price/bit/:qurl', controllers.price.deleteBit)
    app.post('/price/allBit', controllers.price.setBits)
    app.get('/price/search/:searchString', controllers.price.searchPrice)

    app.get('*', function (req, res) {
        res.render('index', {currentUser: req.user});
    });
};