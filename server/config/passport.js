var passport = require('passport'),
    LocalPassport = require('passport-local');

var user = {username:'benny',id:'12334567890'}
module.exports = function() {
    passport.use(new LocalPassport(function(username, password, done) {
        
        if (username == 'benny' && password == 'c11111111') {
            return done(null,user)
        }
        else
        {
            return done(null,false)
        }
    }));

    passport.serializeUser(function(user, done) {
        return done(null,'12345667890')
    });

    passport.deserializeUser(function(id, done) {
        if (id == '12345667890') {
            return done(null,user)
        }
        else
        {
            return done(null,false)
        }
    });
};